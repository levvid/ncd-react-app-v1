import styled from 'styled-components';
import { Button } from 'antd';

export const NcdPrimaryButton=styled(Button)`
  border-radius: 10px;
  padding: 0px 40px;
  margin: 10px 0px;
  @media only screen and (max-width: 768px) {
    padding: 0px 15px;
  }
`;

export const NcdSecondaryButton=styled(Button)`
  border-radius: 10px;
  padding: 0px 15px;
  background: rgb(246,246,246);
  margin: 10px 0px;
  text-transform: uppercase;
  color: rgb(0,73,122);
  font-size: 13.5px;
  font-weight: 600;
  
`;
