import "./style.css";

export const PreferredBadge = ({ style }) => (
  <div class="badge">
    &nbsp;(Preferred)
  </div>
);

export const CircledNumber = ({ style, number }) => (
  <div class="circle">
    {number}
  </div>
);