import styled from 'styled-components';
import {Select, Input, DatePicker, Radio} from 'antd';
import SelectDatePicker from "@netojose/react-select-datepicker";

export const NcdSelect = styled(Select)`
    box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;
`;

export const NcdInput = styled(Input)`
    box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;
`;

export const NcdDatePicker = styled(DatePicker)`
    box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;
`;

export const NcdSelectDatePicker = styled(SelectDatePicker)`
    select {
        padding-right: 0px;
    }
`;