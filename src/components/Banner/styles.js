import styled from 'styled-components';
import { Card ,Row, Button, Input, Divider, Col } from 'antd';

export const Container = styled.div`
  float: none;
  margin: 5px auto 100px;
  min-height: 80vh;
  position: relative;
  width: 90%;
  display: flex;
  background-color: #ffffff;
  justify-content: center;
  align-items: flex-start;
  padding: 10px 15px 20px;
  text-align: center;
  overflow: hidden;
  max-width: 1300px;
`;

export const NcdCard = styled(Card)`
  border: none;
  box-shadow: none;
`;
export const NcdCardContainer=styled(Row)`
  display: flex;
  padding-top: 0px;
  border-radius: 10px;
  background:#ffffff;
  opacity: 1;
  justify-content: left;
  position: relative;
  width: 100%;
`;

export const NcdRadioButton = styled(Button)`
    background-color: white;
    color: #53C3E9;
    border-radius: 10px;
    width: 90%;
`;

export const CartCard = styled(Card)`
  margin: 7px 0px 20px 0px;
  .ant-card-body {
    padding: 20px 10px 10px 10px;
  }
`;

export const CartButton = styled(Button)`
  /* background-color: #02477b; */
  background-color: #fa8406;
  background: linear-gradient(#df5829, #f99e21) !important;
  width: 100%;
  margin: 10px 0px 0px 0px;
`;

export const NcdInput = styled(Input)`
  .input.ant-input {
    margin-left: 20px;
    background-color: white;
  }
`;

export const NcdCardDesc = styled.div`
  min-height: 160px;
  font-size: 12px;
  transition: 1s all;
  ul {
    padding: 10px;
    padding-bottom: 0px;
    list-style-type:none;
    margin: 0px;
  }
`;

export const NcdDivider = styled(Divider)`
  .ant-divider-inner-text {
    padding: 0px;
  }
  &.ant-divider-horizontal.ant-divider-with-text-left::before {
    width: 0%;
  }
`;

export const NcdStepperElement = styled(Col)`
  border-left: 2px solid #4ec0e8;
  margin-left: 12px;
`;