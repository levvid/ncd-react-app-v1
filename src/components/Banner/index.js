import { useEffect } from "react";
import * as S from "./styles";
import ProductForm from './forms/productForm';

const Banner = ({ t }) => {
    return (
        <S.Container>
            <ProductForm />
        </S.Container>
    );
};

export default Banner;
