import React, { lazy, useState, useEffect } from 'react';
import {
    Row,
    Col,
    Radio,
    Form,
    Divider,
    Spin,
    message,
    Result,
} from "antd";
import queryString from "query-string";
import * as S from '../styles';
import axios from 'axios';
import emailValidator from 'email-validator';
import {NcdSelectWithIcon} from '../../../common/Input';
import ChatCard from '../../Chat';
import statesList from '../../../assets/data/statesList';
import {useStore} from '../../../Store';
import configData from '../../../config';

const Expand = lazy(() => import('react-expand-animated'));
const SvgIcon = lazy(() => import("../../../common/SvgIcon"));
const PlanForm = lazy(() => import('./planForm'));

const ProductForm = ({}) => {
    const apiResponse = useStore(state => state.apiResponse);
    const {
        searchFields, 
        setSearchFields,
        planForm,
        setPlanForm,
    } = useStore(state => ({
        searchFields: state.searchFields,
        setSearchFields: state.setSearchFields,
        planForm: state.planForm,
        setPlanForm: state.setPlanForm,
    }));

    const [form] = Form.useForm();
    const [formLayout, setFormLayout] = useState('vertical');
    const [expand, setExpand] = useState(true);
    const [product, setProduct] = useState('both');
    const [isLoading, setIsLoading] = useState(false);
    const [forceUpdate, setForceUpdate] = useState(0);

    useEffect(async () => {
        let parsed = queryString.parse(window.location.search);
        let search_state = parsed.state;
        let search_product_type = parsed.type;
        if (search_state !== undefined) {
            form.setFieldsValue({search_state: search_state});
            if (search_product_type === undefined) {
                monitorStateChange(search_state);
            } else {
                form.setFieldsValue({search_product_type: search_product_type});
                form.submit();
            }
        }

        if (searchFields.search_product_type && searchFields.search_product_type != product && searchFields.search_product_type === "both") { 
            setProduct(searchFields.search_product_type);
            form.setFieldsValue({search_product_type: 'both'}); 
          }

    }, [searchFields.search_product_type]);

    useEffect(async () => {
        try {
            let parsed = queryString.parse(window.location.search);
            if (parsed.state !== undefined) {
                return;
            }
            let r = await axios({
                method: 'GET',
                url: `${configData.API_URL}/geo`,
            });
            if (r.data.country_code === 'US' && apiResponse['states'].hasOwnProperty(r.data.region_code)) {
                const stateProducts = apiResponse['states'][r.data.region_code];
                form.setFieldsValue({search_state: r.data.region_code});
                if (stateProducts.length === 1) {
                    form.setFieldsValue({search_product_type: 'vision'});
                } else if (stateProducts.length === 3) {
                    form.setFieldsValue({search_product_type: 'dental'});
                } else {
                    form.setFieldsValue({search_product_type: 'both'});
                }
                form.submit();
            }
        } catch (e) {
            return;
        }
    }, []);

    const onFinish = async (values) => {
        setIsLoading(true);
        let flag = 0;
        try {
            if (apiResponse['states'].hasOwnProperty(values.search_state)) {
                const stateProducts = apiResponse['states'][values.search_state];
                if (values.search_product_type === 'dental') {
                    if (stateProducts.length >= 3) {
                        flag = 1;
                    }
                }
                if (values.search_product_type === 'vision') {
                    if (stateProducts.length === 1 || stateProducts.length === 4) {
                        flag = 1;
                    }
                }
                if (values.search_product_type === 'both') {
                    if (stateProducts.length >= 4) {
                        flag = 1;
                    }
                }
                if (flag) {
                    setSearchFields(values);
                    setProduct(values.search_product_type);
                    setExpand(true);
                } else {
                    setExpand(false);
                    setProduct('404');
                }
            } else {
                setExpand(false);
                setSearchFields(values);
                setProduct('404');
            }
        } catch (e) {
            message.error('Unable to load products');
        }
        setForceUpdate(1 - forceUpdate);
        setIsLoading(false);
    };

    const monitorStateChange = (value) => {
        let temp = planForm;
        temp["dental_benefits"] = 51;
        temp['vision_benefits'] = 51;
        temp["dental_package_type"] = 3;
        setPlanForm(temp);
        if (apiResponse['states'][value]?.length === 1) {
            form.setFieldsValue({search_product_type: 'vision'});
        }
        if (apiResponse['states'][value]?.length >= 3) {
            form.setFieldsValue({search_product_type: 'both'});
        }
        if (!(value in apiResponse['states']) || apiResponse['states'][value]?.length === 0) {
            form.setFieldsValue({search_product_type: '404'});
        }
        form.submit();
        window.scrollTo(0, 0);
    };

    const handleEmailBlur = async (e) => {
        if (emailValidator.validate(e.target.value)) {
            setSearchFields({
                ...searchFields,
                banner_email: e.target.value
            });
            let r = await axios({
                method: 'POST',
                url: `${configData.API_URL}/email`,
                data: {
                    email: e.target.value,
                }
            });
        } else {
            return;
        }
    };

    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24,
            },
            xl: {
                span: 24
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            xl: {
                span: 20
            }
        }
    };

    return (
        <div style={{width: '100%'}}>
            <Form
                {...formItemLayout}
                layout={formLayout}
                form={form}
                initialValues={{
                    
                }}
                onFinish={onFinish}
                onFinishFailed={() => {
                    form.setFieldsValue({product: 'unchecked'});
                    }
                }
                style={{width: '100%', marginBottom: 0}}
                size="large"
            >
                <Row gutter={24} align="middle" justify="left">
                    <Col xs={24} xl={6}>
                        <Form.Item
                            label="State:"
                            name='search_state'
                            rules={
                                [
                                    {
                                        required: true,
                                        message: 'Please select state'
                                    }
                                ]
                            }
                        >
                            <NcdSelectWithIcon
                                icon={<SvgIcon src="map.png" height={25} />}
                                dropdownItems={statesList}
                                onCustomChange={monitorStateChange}
                                placeholder="Select State"
                            />
                        </Form.Item>
                    </Col>
                    <Col xl={9} xs={24}>
                        <Form.Item
                            label="Email: "
                            name="banner_email"
                            rules={
                                [
                                    {
                                        required: false,
                                        type: "email",
                                        message: 'Please enter a valid email.'
                                    }
                                ]
                            }
                        >
                            <S.NcdInput
                            placeholder="Enter email" 
                            prefix={<SvgIcon src="envelope.png" height={30} />}
                            onBlur={handleEmailBlur}
                            />
                        </Form.Item>
                    </Col>
                    <Col xl={9} xs={24}>
                            <Form.Item
                                label="Select products:"
                                name="search_product_type"
                                wrapperCol={{sm:24}}
                                rules={
                                    [
                                        {
                                            required: true,
                                            message: 'Please select products'
                                        }
                                    ]
                                }
                            >
                                <Radio.Group
                                    className="ant-input-affix-wrapper"
                                    style={{padding: '7px 0px', fontWeight: 600}}
                                    onChange={(e) => {
                                        form.submit();
                                    }}
                                    value={product}
                                    defaultValue={product}
                                >
                                    <Radio  value='dental'>
                                        <SvgIcon src="dental.png" height={30}/>
                                        Dental
                                    </Radio>
                                    <Radio  value='vision'>
                                        <SvgIcon src="vision.png" height={30} />
                                        &nbsp;Vision
                                    </Radio>
                                    <Radio  value='both'>Both</Radio>
                                </Radio.Group>
                            </Form.Item>
                    </Col>
                </Row>
            </Form>
            <Divider style={{marginTop: 0,}} />
            <br />
            <Expand open={isLoading}>
                <Spin size="large" />
            </Expand>
            <Expand open={expand}>
                <PlanForm categ={product} forceUpdate={forceUpdate} />
            </Expand>

            <Expand open={(product === '404')}>
                <Row gutter={24}>
                    <Col xl={16}>
                        <Result
                            icon={<SvgIcon src="pnf.svg" />}
                            title={
                            <div style={{fontSize: 18}}>
                                Our products are currently unavailable in your state. 
                                Please drop us a line if you would like to help us change that:&nbsp;
                                <a href='mailto:info@nationalcaredental.com' target='_blank'>info@nationalcaredental.com</a>
                            </div>
                            }
                        />
                    </Col>
                    <Col xl={8}>
                        <ChatCard />
                    </Col>
                </Row>
                             
            </Expand>
        </div>
    );
}

export default ProductForm;
