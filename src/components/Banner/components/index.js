import {lazy, useState} from 'react';
import CardGroup from '../../../common/CardGroup';

const SvgIcon = lazy(() => import("../../../common/SvgIcon"));

const dentalPlanContent = [
    {
        body: (
            <div className="card-desc">
                <SvgIcon src='member.png' height={55} />
                <br />
                <b>Member</b>
            </div>
        ),
        value: 51
    },
    {
        body: (
            <div className="card-desc">
                <SvgIcon src='member_spouse.png' height={55} />
                <br />
                <b>Member + Spouse</b>
            </div> 
        ),
        value: 41
    },
    {
        body: (
            <div className="card-desc">
                <SvgIcon src='member_children.png' height={55} />
                <br />
                <b>Member + Children</b>
            </div> 
        ),
        value: 677
    },
    {
        body: (
            <div className="card-desc">
                <SvgIcon src='member_family.png' height={55} />
                <br />
                <b>Family</b>
            </div> 
        ),
        value: 21
    }
];

const visionPlanContent = [
    {
        body: (
            <div className="card-desc">
                <SvgIcon src='member.png' height={55} />
                <br />
                <b>Member</b>
            </div> 
        ),
        value: 51
    },
    {
        body: (
            <div className="card-desc">
                <SvgIcon src='member_one.png' height={55} />
                <br />
                <b>Member + 1</b>
            </div> 
        ),
        value: 450
    },
    {
        body: (
            <div className="card-desc">
                <SvgIcon src='member_family.png' height={55} />
                <br />
                <b>Family</b>
            </div> 
        ),
        value: 21
    }
];

export const DentalPlanSelect = ({valueCallback, validationError, reset, defaultState}) => (
    <CardGroup 
        cardsContent={dentalPlanContent} 
        xl_default={6}
        errorMessage="Please select dental benefits"
        valueCallback={valueCallback}
        validationError={validationError}
        reset={reset}
        defaultState={defaultState}
    />
);

export const VisionPlanSelect = ({valueCallback, validationError, reset, defaultState}) => (
    <CardGroup 
        cardsContent={visionPlanContent} 
        xl_default={6}
        errorMessage="Please select vision benefits"
        valueCallback={valueCallback}
        validationError={validationError}
        reset={reset}
        defaultState={defaultState}
    />
);