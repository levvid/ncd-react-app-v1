import { useState, Fragment, lazy } from "react";
import { Row, Col } from "antd";

import * as S from "./styles";

const SvgIcon = lazy(() => import("../../common/SvgIcon"));

const Header = ({ t }) => {
  const isMobile = window.innerWidth <= 768;

  return (
    <S.Header>
      <S.Container>
        <Row type="flex" justify="space-between" gutter={24}>
          <Col xl={16} xs={10}>
            <S.LogoContainer aria-label="homepage">
              <Row type='flex' gutter={24} align='middle' style={{height: '100%'}}>
                <Col xl={6}>
                  <SvgIcon src="logo.svg" 
                    height={100}
                  />
                </Col>
                <Col xl={2} style={{marginRight: 5}} className='extra-logos'>
                <p style={{fontSize: 8}}>Dental underwritten by</p>
                </Col>
                <Col xl={5} className='extra-logos'>
                  <SvgIcon src="nationwide_logo.svg" style={{verticalAlign: 'bottom'}} 
                    height={50}
                  />
                </Col>
                <Col xl={2} style={{marginRight: 5}} className='extra-logos'>
                <p style={{fontSize: 8}}>Vision underwritten by</p>
                </Col>
                <Col xl={5} className='extra-logos'>
                  <SvgIcon src="vsp_logo.svg" style={{verticalAlign: 'bottom'}} 
                    height={50}
                  />
                </Col>
              </Row>
            </S.LogoContainer>
          </Col>
          <Col xl={8} xs={14}>
            <S.CallBox>
              <span class="light-span">Need help?</span>
              <h3><b>(888)-415-4801</b></h3>
              <h5>Mon-Fri 8am-6pm(CST)</h5>
            </S.CallBox>
          </Col>
        </Row>
      </S.Container>
    </S.Header>
  );
};

export default Header;
