import {
    Button,
    Result,
    Typography,
} from "antd";
import { Link } from "react-router-dom";
import { CheckOutlined } from "@ant-design/icons";

const { Paragraph, Text } = Typography;

const SuccessPage = () => (
  <Result
    status="success"
    title="Application submitted successfully"
    extra={[
      <Link to="/">
        <Button type="primary">Start a new application</Button>
      </Link>,
    ]}
  >
    <div className="desc">
      <Paragraph>
        <Text
          strong
          style={{
            fontSize: 16,
          }}
        >
          You will hear from us soon, in the meantime you can:
        </Text>
      </Paragraph>
      <Paragraph>
        <CheckOutlined className="site-result-demo-error-icon" /> Read more
        about our offerings <Link to="">here</Link>
      </Paragraph>
      <Paragraph>
        <CheckOutlined className="site-result-demo-error-icon" /> Mail us at{" "}
        <a href="mailto:info@nationalcaredental.com">
          info@nationalcaredental.com
        </a>{" "}
        if you have any query.
      </Paragraph>
    </div>
  </Result>
);

export default SuccessPage;
