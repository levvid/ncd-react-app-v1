import { Divider, Button, Modal, Row, Col } from "antd";

export default function CheckOutModal({
  isVisible,
  setIsVisible,
  customFunction,
}) {

  const handleOk = async () => {
    customFunction(0);
    setIsVisible(false);
  };

  const handleCancel = async () => {
    customFunction(1);
    setIsVisible(false);
  };

  return (
    <>
      <Modal
        title="Important notice"
        visible={isVisible}
        centered
        width={500}
        style={{ padding: '10px', top: 0}}
        onCancel={handleCancel}
        footer={[
          <Button key="cancel" onClick={handleOk}>
            Fill Info
          </Button>,
          <Button key="submit" type="primary" onClick={handleCancel}>
            Proceed
          </Button>,
        ]}
      >
          If you have enrolled in one of our dental plans, your plan includes access to 3 national networks: Dentemax, Careington, and Connection Dental. All benefits for covered preventative and basic services begin on your plan active date. There are no waiting periods for covered preventative and basic dental services. There is a 12-month waiting period for covered major dental services. If you had previous PPO or MAC dental coverage prior to your enrollment in the NCD dental plan and did not have a break in coverage, you may be able to get credit for the number of months you had continuous coverage toward the NCD major services 12-month waiting period. If your proof of prior coverage was not selected at the time of enrollment, it must be submitted to Member Care within 30 days of the plan active date.
      </Modal>
    </>
  );
}
