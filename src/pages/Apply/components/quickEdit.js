import { lazy, useState, useEffect } from "react";
import { Divider, Button, Input, Modal, Form, Result, Row, Col, message } from "antd";
import { NcdSecondaryButton } from "../../../common/Button";
import axios from "axios";
import { useStore } from "../../../Store";
import * as S from "./styled.js";
import { DeleteOutlined } from "@ant-design/icons";
import {DentalQuickSelect, VisionQuickSelect} from './quickCartSelects';
import {determineDependentCount, determineDependentInfo } from './methods';
import { SpouseInfo, ChildInfo } from "./dependentInfo";
import * as M from '../../../components/Banner/forms/methods';
import parse from "html-react-parser";
import configData from "../../../config";

const Expand = lazy(() => import('react-expand-animated'));
const SvgIcon = lazy(() => import("../../../common/SvgIcon"));

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
    xl: {
      span: 24,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
    xl: {
      span: 24,
    },
  },
};

const QuickEdit = (props) => {
  const { isVisible, setIsVisible, setIsAddDentalPreviousCoverageModal } = props;

  const [form] = Form.useForm();
  const [formLayout, setFormLayout] = useState("vertical");

  const {cartOverview, 
    searchFields, 
    planForm, 
    apiResponse, 
    setCartOverview, 
    setSearchFields, 
    setPlanForm, 
    applicantForm, 
    setApplicantForm,
    appID,
    setCurrent,
    current,
    setPreviousInsuranceForm,
  } = useStore((state) => ({
    cartOverview: state.cartOverview,
    searchFields: state.searchFields,
    planForm: state.planForm,
    apiResponse: state.apiResponse,
    setCartOverview: state.setCartOverview,
    setSearchFields: state.setSearchFields,
    setPlanForm: state.setPlanForm,
    applicantForm: state.applicantForm,
    setApplicantForm: state.setApplicantForm,
    appID: state.appID,
    current: state.current, 
    setCurrent: state.setCurrent,
	  setPreviousInsuranceForm: state.setPrevInsuranceForm,
  }));

  	let childAdder = null;
  	let spouseAdder = null;
	

  const [tempCartOverview, setTempCartOverview] = useState(cartOverview);
	const [tempSearchFields, setTempSearchFields] = useState(searchFields);
	const [tempPlanForm, setTempPlanForm] = useState(planForm);

	const [isDentalSelectExpanded, setIsDentalSelectExpanded] = useState(false);
	const [isVisionSelectExpanded, setIsVisionSelectExpanded] = useState(false);
	const [isLoading, setIsLoading] = useState(false);
	
	const [showAddChild, setShowAddChild] = useState(false); 
	const [showAddSpouse, setShowAddSpouse] = useState(false);
  const [showOnlyOne, setShowOnlyOne] = useState(false);
  
  useEffect(() => {
    form.setFieldsValue(applicantForm);
  }, [applicantForm]);


  useEffect(() => {
    let dependentInfo = determineDependentInfo(tempPlanForm, tempSearchFields);
    setShowAddSpouse(dependentInfo[0]);
    setShowAddChild(dependentInfo[1]);
    setShowOnlyOne(dependentInfo[2]);
  }, [tempPlanForm]);
	
  const isMemberOnly = (overview) => {
    let isMember = true;
    overview.cartProducts.forEach(element => {
      if('benefitID' in  element) {
        if (element['benefitID'] !== 51){
          isMember =  false;
        }
      }
    });
    return isMember;  
  }


  const updateApplicantForm = (values, overview) => { 
    if(Object.keys(values).length === 0 || isMemberOnly(overview)) {
      // delete dependents
      setApplicantForm({
        ...applicantForm,
        dependent_children: [],
        dependent_spouse: []
      });
    } else {
      setApplicantForm({
        ...applicantForm,
        ...values
      });
    }
  }

  const handleOk = async (values) => {
    setIsLoading(true);
    if (tempSearchFields.search_product_type === 'none') {
      setIsLoading(false);
      return message.error('Cart cannot be empty');
    }
    updateApplicantForm(values, tempCartOverview);
    // check if applicantDetails have been set or not
    // if (applicantForm.email === undefined) {
    //   setIsLoading(false);
    //   return setIsVisible(false);
    // }
    try{
        let res = await axios({
          method: "patch",
          url: `${configData.API_URL}/app`,
          data: {
            app_data: {
              applicantForm: {
                ...applicantForm,
                ...values
              },
            },
            app_id: appID,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
    } catch(e) {
        console.log("Quick edit save failed");
        console.log(e);
    }
	  
	  // adding or deleting dental - change currentPage - GM
    if (['both', 'dental'].includes(searchFields.search_product_type) && !['both', 'dental'].includes(tempSearchFields.search_product_type)) {  // dental deleted
      console.log('Dental deleted: ' + current);
      if (current > 1) {
        setCurrent(current - 1);
      }

      // reset previous insurance 
      setPreviousInsuranceForm({});
    } else if(!['both', 'dental'].includes(searchFields.search_product_type) && ['both', 'dental'].includes(tempSearchFields.search_product_type)) { // dental added
      console.log('Adding dental: ' + current);
      if (current > 0) {
        console.log("Current adding: " + current);
        setCurrent(current + 1); 
        setIsAddDentalPreviousCoverageModal(true);
      }      
    }
    
    M.handleCart(tempSearchFields, apiResponse, tempPlanForm, setCartOverview);
    setSearchFields(tempSearchFields);
    setPlanForm(tempPlanForm);
    message.success('Cart updated');
    setIsLoading(false);
    setIsVisible(false);
    stopExpandingDentalAndVision();

  };

  const stopExpandingDentalAndVision = () => { 
    setIsDentalSelectExpanded(false); 
    setIsVisionSelectExpanded(false);
  }

  const handleCancel = () => {
    setTempSearchFields(searchFields); 
    setTempCartOverview(cartOverview); 
    setTempPlanForm(planForm);
    stopExpandingDentalAndVision();
    setIsVisible(false);
  };

  const handleExpand = (type, setExpand) => {
    let tmpPlanForm = JSON.parse(JSON.stringify(tempPlanForm))  ;
    if (type == 'dental') { 
      // default package type to 3000+ - 3
      tmpPlanForm['dental_package_type'] = 3;
      // default benefits to member - 51
      tmpPlanForm['dental_benefits'] = 51;
    } else if (type == 'vision' ){ // vision
      // default to member - 51
      tmpPlanForm['vision_benefits'] = 51;
    } else {
      console.log('Trying to add unmapped type.')
    }
    
    let tmp = JSON.parse(JSON.stringify(tempSearchFields));
    tmp['search_product_type'] = (tempSearchFields.search_product_type === 'none') ? type : 'both';
    setTempSearchFields(tmp);

    setTempPlanForm(tmpPlanForm);
    M.handleCart(tmp, apiResponse, tmpPlanForm, setTempCartOverview);
    setExpand(true);
  };

  const getComplement = (type) => {
    if (type === 'dental') {
      return 'vision';
    }
    return 'dental';
  };

  return (
    <>
      <Modal
        title={
          <>
            <SvgIcon src="cart.png" height={20} /> Update Cart
          </>
        }
        destroyOnClose={true}
				width='80%'
        visible={isVisible}
        onCancel={() => setIsVisible(false)}
        footer={[
          <Button key="cancel" onClick={handleCancel}>
            Cancel
          </Button>,
          <Button key="submit" 
            type="primary" 
            onClick={() => form.submit()}
            loading={isLoading}
          >
            Update Cart
          </Button>,
        ]}
      >
        <>
          {tempCartOverview.cartProducts.map((element) => (
            <>
              {(tempSearchFields.search_product_type === 'both' && ['dental', 'vision'].includes(element.type)) || tempSearchFields.search_product_type === element.type ? (
                <S.DepBox>
                  <Row gutter={24}>
                    <Col xl={2}>
                      <SvgIcon src={`${element.type}.png`} height={20} />
                    </Col>
                    <Col xl={14}>
                      <b>{parse(element.label)}</b>
                    </Col>
                    <Col xl={4}>${element.price}</Col>
                    <Col xl={4}>
                      <DeleteOutlined
                        onClick={() => {
                          setTempSearchFields({
                            ...tempSearchFields,
                            search_product_type: (tempSearchFields.search_product_type === 'both') ? getComplement(element.type) : 'none'
                          });
                        }}
                        style={{ fontSize: 25 }}
                      />
                    </Col>
                  </Row>
                </S.DepBox>
              ) : null}
            </>
          ))}
					<Divider />
					<Row gutter={24} justify='center'>
					  <Col xl={20}>
							<Expand open={isDentalSelectExpanded}>
							  <DentalQuickSelect 
                  setIsExpanded={setIsDentalSelectExpanded}
                  tempPlanForm={tempPlanForm}
                  setTempPlanForm={setTempPlanForm}
                  tempSearchFields={tempSearchFields}
                  setTempSearchFields={setTempSearchFields}
                  setTempCartOverview={setTempCartOverview}
                />
							</Expand>
						</Col>
					</Row>
          <Row gutter={24} justify='center'>
					  <Col xl={20}>
							<Expand open={isVisionSelectExpanded}>
							  <VisionQuickSelect 
                  setIsExpanded={setIsVisionSelectExpanded}
                  tempPlanForm={tempPlanForm}
                  setTempPlanForm={setTempPlanForm}
                  tempSearchFields={tempSearchFields}
                  setTempSearchFields={setTempSearchFields}
                  setTempCartOverview={setTempCartOverview}
                />
							</Expand>
						</Col>
					</Row>
          <Row gutter={24}>
            {(tempSearchFields.search_product_type === 'vision' || tempSearchFields.search_product_type === 'none')
            && (apiResponse['states'][searchFields.search_state].length >= 3) ? (
              <Col xl={6}>
                <NcdSecondaryButton onClick={() => handleExpand('dental', setIsDentalSelectExpanded)}
                >
                  Add Dental Product
                </NcdSecondaryButton>
              </Col>
            ) : null}
            {(tempSearchFields.search_product_type === 'dental' || tempSearchFields.search_product_type === 'none')
            && (apiResponse['states'][searchFields.search_state].length >= 4 || apiResponse['states'][searchFields.search_state].length === 1) ? (
              <Col xl={6}>
                <NcdSecondaryButton onClick={() => handleExpand('vision', setIsVisionSelectExpanded)}
									>
										Add Vision Product
								</NcdSecondaryButton>
              </Col>
            ) : null}
            
          </Row>
          <Row gutter={24}>
            {showAddChild || showAddSpouse ? ( 
              <Divider orientation='left'>
                Dependants
              </Divider>
            ): null}
          </Row> 
          <Form
            {...formItemLayout}
              form={form}
              formLayout={formLayout}
              name="applicant"
              size="large"
              scrollToFirstError={true}
              onFinish={handleOk}
              initialValues={applicantForm}
          >
            <Row>
              {showAddSpouse ? (
                <SpouseInfo
                  setAddHandler={(f) => (spouseAdder = f)}
                  removeEffects={() => {
                    if(showOnlyOne) {
                      setShowAddChild(true);
                    }
                  }}
                  form={form}
                />
              ) : null}
            </Row>
            <Row>
              {showAddChild ? (
                <ChildInfo
                setAddHandler={(f) => (childAdder = f)}
                removeEffects={() => {
                  if(showOnlyOne && (!form.getFieldValue('dependent_children') || form.getFieldValue('dependent_children').length === 0)) {
                    setShowAddSpouse(true);
                  }
                }}
              />
              ): null}
            </Row>
            <Row gutter={24}>
              {showAddSpouse 
              && (!form.getFieldValue('dependent_spouse') || form.getFieldValue('dependent_spouse').length === 0) ? (
                <Col xs={12} xl={6}>
                  <NcdSecondaryButton
                    onClick={() => {
                      spouseAdder();
                      if (showOnlyOne) {
                        setShowAddChild(false);
                      }
                    }}
                  >
                    <SvgIcon src='member_spouse.png' height='24' />&nbsp;Add Spouse
                  </NcdSecondaryButton>
                </Col>
              ) : null}
              {showAddChild ? (
                <Col xs={12} xl={6}> 
                  <NcdSecondaryButton
                    onClick={() => {
                      childAdder();
                      if (showOnlyOne) {
                        setShowAddSpouse(false);
                      }
                    }}
                  >
                    <SvgIcon src='member_children.png' height='24' />&nbsp;Add Child
                  </NcdSecondaryButton>
                </Col>
              ) : null}
            </Row>
          </Form>
        </>
      </Modal>
    </>
  );
};

export default QuickEdit;
