import {useEffect} from 'react';
import { Divider, Button, Input, Modal, Form, Result, Row, Col, message} from "antd";
import { NcdSecondaryButton } from "../../../common/Button";
import {DentalPlanSelect, VisionPlanSelect} from '../../../components/Banner/components/index.js';
import { useStore } from "../../../Store";
import PricingCard from '../../../components/Banner/components/pricingCards';
import {dentalPricingData} from '../../../assets/data/pricingCardData';
import * as M from '../../../components/Banner/forms/methods';

export const DentalQuickSelect = (props) => {
    const {apiResponse} = useStore(state => ({apiResponse: state.apiResponse}));

    const {
        setIsExpanded, 
        tempPlanForm, 
        setTempPlanForm, 
        tempSearchFields, 
        setTempSearchFields,
        setTempCartOverview,
    } = props;

    const handleOk = () => {
        try {
            M.handleCart(tempSearchFields, apiResponse, tempPlanForm, setTempCartOverview);
        } catch (e) {
            message.error('Error adding products to cart');
        }
        setIsExpanded(false);
    };

    const handleCancel = () => {
        let tmp = JSON.parse(JSON.stringify(tempSearchFields));
        tmp['search_product_type'] = (tempSearchFields.search_product_type === 'both') ? 'vision' : 'none';
        M.handleCart(tmp, apiResponse, tempPlanForm, setTempCartOverview);
        setTempSearchFields({
            ...tempSearchFields,
            search_product_type: (tmp.search_product_type === 'both') ? 'vision' : 'none'
        });
        setIsExpanded(false);
    };

    const handlePricing = (value) => {
        let tmp = JSON.parse(JSON.stringify(tempPlanForm)); // MLG
        tmp['dental_package_type'] = value; // MLG
        M.handleCart(tempSearchFields, apiResponse, tmp, setTempCartOverview); // MLG
        setTempPlanForm({
            ...tempPlanForm,
            dental_package_type: value
        });
    };

    const handleBenefits = (value) => {
        let tmp = JSON.parse(JSON.stringify(tempPlanForm)); // MLG
        tmp['dental_benefits'] = value; // MLG
        M.handleCart(tempSearchFields, apiResponse, tmp, setTempCartOverview); // MLG
        setTempPlanForm({
            ...tempPlanForm,
            dental_benefits: value
        });
    };

    return (
        <>  
            <h3>Add Dental Product</h3>
            <Row gutter={24} align='top' justify='start'>
                <Col xl={24}>
                        <Divider orientation='left'>
                            Select Dental Products
                        </Divider>
                        <PricingCard
                            valueCallback={handlePricing}
                            pricingData={dentalPricingData}
                            defaultState={3}
                        />
                </Col>
            </Row>
            <Row gutter={24}>
                <Col xl={24}>
                    <Divider orientation='left'>
                        Dental Plan Benefits
                    </Divider>
                    <DentalPlanSelect 
                        valueCallback={handleBenefits}
                        defaultState={51}
                    />
                </Col>
            </Row>
            <Row gutter={24} justify='end' style={{marginTop: 20}}>
                <Col xl={4}>
                    <Button style={{width: '100%'}} onClick={handleCancel}>Cancel</Button>
                </Col>
                <Col xl={4}>
                    <Button type='primary' style={{width: '100%'}} onClick={handleOk}>Add</Button>
                </Col>
            </Row>
        </>
    );
};

export const VisionQuickSelect = (props) => {
    const {apiResponse} = useStore(state => ({apiResponse: state.apiResponse}));

    const {
        setIsExpanded, 
        tempPlanForm, 
        setTempPlanForm, 
        tempSearchFields, 
        setTempSearchFields,
        setTempCartOverview,
    } = props;

    const handleOk = () => {
        try {
            M.handleCart(tempSearchFields, apiResponse, tempPlanForm, setTempCartOverview);
        } catch (e) {
            message.error('Error adding products to cart');
        }
        setIsExpanded(false);
    };

    const handleCancel = () => {
        let tmp = JSON.parse(JSON.stringify(tempSearchFields)); // MLG
        tmp['search_product_type'] = (tempSearchFields.search_product_type === 'both') ? 'dental' : 'none'; // MLG
        M.handleCart(tmp, apiResponse, tempPlanForm, setTempCartOverview); // MLG
        
        setTempSearchFields({
            ...tempSearchFields,
            search_product_type: (tempSearchFields.search_product_type === 'both') ? 'dental' : 'none'
        });
        setIsExpanded(false);
    };

    const handleBenefits = (value) => {
        let tmp = JSON.parse(JSON.stringify(tempPlanForm)); // MLG
        tmp['vision_benefits'] = value; // MLG
        M.handleCart(tempSearchFields, apiResponse, tmp, setTempCartOverview); // MLG
        setTempPlanForm({
            ...tempPlanForm,
            vision_benefits: value
        });
    };

    return (
        <>  
            <h3>Add Vision Product</h3>
            <Row gutter={24}>
                <Col xl={24}>
                    <Divider orientation='left'>
                        Vision Plan Benefits
                    </Divider>
                    <VisionPlanSelect 
                        valueCallback={handleBenefits}
                        defaultState={51}
                    />
                </Col>
            </Row>
            <Row gutter={24} justify='end' style={{marginTop: 20}}>
                <Col xl={4}>
                    <Button style={{width: '100%'}} onClick={handleCancel}>Cancel</Button>
                </Col>
                <Col xl={4}>
                    <Button type='primary' style={{width: '100%'}} onClick={handleOk}>Add</Button>
                </Col>
            </Row>
        </>
    );
};
