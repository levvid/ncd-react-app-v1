import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Form, Input, Row, Col, Checkbox, Divider, Button } from "antd";
import * as S from "../../styles";
import configData from "../../../../config";
import { useStore } from "../../../../Store";
import { FileTextOutlined } from "@ant-design/icons";
import SummaryModal from "../summaryModal";
import CheckOutModal from '../checkOutModal';
import TermsAndConditionsModal from '../termsAndConditionsModal';
import { termscontent, ncdVisionAgreement, ncdMemberAgreement, nsbaAgreement, ncdDentalAgreement } from "../../../../assets/data/termsContent";

import SignatureCanvas from 'react-signature-canvas';
import SignaturePad from 'react-signature-canvas';
import parse from "html-react-parser";


const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
    xl: {
      span: 24,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
    xl: {
      span: 24,
    },
  },
};

const SummaryForm = (props) => {
  const {
    applicantForm,
    searchFields,
    planForm,
    cartOverview,
    appID,
    prevInsuranceForm,
    paymentInfoForm,
    setPaymentInfoForm,
  } = useStore((state) => ({
    applicantForm: state.applicantForm,
    searchFields: state.searchFields,
    planForm: state.planForm,
    cartOverview: state.cartOverview,
    appID: state.appID,
    prevInsuranceForm: state.prevInsuranceForm,
    paymentInfoForm: state.paymentInfoForm,
  }));

  const [form] = Form.useForm();
  const [isReviewModalVisible, setIsReviewModalVisible] = useState(false);
  const [isTermsAndConditionsModalVisible, setIsTermsAndConditionsModalVisible] = useState(false);
  const [isCheckOutModalVisible, setIsCheckOutModalVisible] = useState(false);
  const [signature, setSignature] = useState({});


  const [checked, setChecked] = useState(false);

  const onCheckboxChange = (e) => {
    setChecked(e.target.checked);
  };

  const validation = (rule, value, callback) => {
    if (checked) {
      return callback();
    }
    return callback("Please accept the terms and conditions");
  };

  const signatureValidator = (rule, value, callback) => {
    if (!signature.isEmpty()) {
      return callback();
    }
    return callback("Please provide your signature");
  };
  
  const generateTermsAndConditions = (header, text) => {
    return (
      <>
      <S.termsAndConditionsHeader>{header}</S.termsAndConditionsHeader>
      <S.termsAndConditionsContent>{parse(text)}</S.termsAndConditionsContent>
      </>
    )
  }

  props.setFormHandler(form);

  let applicant_name =
    applicantForm.applicant_fname +
    (applicantForm.applicant_mname ? ` ${applicantForm.applicant_mname}` : "");
  applicant_name = applicant_name + " " + applicantForm.applicant_lname;

  const handleAfterCheckOut = async (success = 1) => {
    if (!success) {
      props.setIsStepSuccessful(false);
      return;
    }
    let values = form.getFieldsValue();

    try {
      let res = await axios({
        method: "patch",
        url: `${configData.API_URL}/app`,
        data: {
          app_data: {
            searchFields: searchFields,
            planForm: planForm,
            cartOverview: cartOverview,
            applicantForm: applicantForm,
            prevInsuranceForm: prevInsuranceForm,
            paymentInfoForm: {},
            isAgent: true,
            signatureUrl: signature.getTrimmedCanvas().toDataURL('image/png')
          },
          app_email: searchFields.banner_email,
          app_id: appID,
          is_complete: 1,
        },
        headers: {
          "Content-Type": "application/json",
        },
      });
      props.setIsStepSuccessful(true);
    } catch (e) {
      props.setIsStepSuccessful(false);
    }
  };

  const onFinish = async (values) => {
    setIsCheckOutModalVisible(true);
  };
  
  const clearSignature = () => {
    signature.clear();
  }

  return (
    <>
      <Form
        {...formItemLayout}
        form={form}
        name="applicant"
        onFinish={onFinish}
        size="large"
        scrollToFirstError
      >
        <Row gutter={12}>
          <Col xl={24} xs={24}>
            <S.headingDiv>
              <S.bluTag />
              <S.headingContent>Purchase Summary</S.headingContent>
            </S.headingDiv>
            <Row gutter={24}>
              <Col xl={24} xs={24}>
                <p
                  style={{
                    color: "black",
                    fontSize: "2.25vh",
                    margin: "30px 0px",
                    display: "flex",
                    justifyContent: "start",
                  }}
                >
                  Please review the information below and submit the application
                </p>
                <S.tableHeadingDiv>
                  <S.tableColHeading>Item(s)</S.tableColHeading>
                  <S.tableColHeading>Amount($)</S.tableColHeading>
                </S.tableHeadingDiv>
                <div>
                  {cartOverview?.displayProducts?.map((productGroup) => (
                    <>
                      {productGroup.map(element => (
                        <S.tableRowDiv>
                         <S.fieldName>{element.labelBrief}</S.fieldName>
                         <S.fieldName fontWeight={"bold"}>
                           ${element.price}
                         </S.fieldName>
                        </S.tableRowDiv>
                      ))}
                      <Divider style={{margin: 8}} />
                    </>
                  ))}
                </div>
                <Divider style={{ margin: "10px" }}></Divider>
                <S.tableFooterDiv>
                  <S.tableFooterContent>
                    Estimated Initial Payment &nbsp;&nbsp;
                    <b>${cartOverview?.cartValue}</b>
                  </S.tableFooterContent>
                </S.tableFooterDiv>
                <div>
                  <S.tableRowDiv>
                    <S.fieldName fontWeight={"bold"}>
                      Ongoing Payment Type
                    </S.fieldName>
                    <S.fieldName>
                      {paymentInfoForm.payment_method_type == "ach"
                        ? "ACH"
                        : "CREDIT CARD"}
                    </S.fieldName>
                  </S.tableRowDiv>
                  <S.tableRowDiv>
                    <S.fieldName fontWeight={"bold"}>
                      Applicant Name
                    </S.fieldName>
                    <S.fieldName>{applicant_name}</S.fieldName>
                  </S.tableRowDiv>
                  <S.tableRowDiv>
                    <S.fieldName fontWeight={"bold"}>
                      Coverage Effective Date
                    </S.fieldName>
                    <S.fieldName>{planForm.cov_eff_date}</S.fieldName>
                  </S.tableRowDiv>
                  <S.tableRowDiv>
                    <S.fieldName fontWeight={"bold"}>
                      First Billing Date
                    </S.fieldName>
                    <S.fieldName>{planForm.cov_bill_date}</S.fieldName>
                  </S.tableRowDiv>
                </div>
                <Divider></Divider>
                <Row gutter={24}>
                  <Col xl={12} xs={12}>
                    <S.headingContent style={{ marginLeft: "0" }}>
                      Your Application Information
                    </S.headingContent>
                  </Col>
                  <Col xl={12} xs={12}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "start",
                        cursor: "pointer",
                      }}
                    >
                      <FileTextOutlined
                        style={{ fontSize: "20.5px", color: "#008fc0" }}
                      />
                      <p
                        style={{ fontSize: "14.5px", marginLeft: "5px" }}
                        onClick={() => {
                          setIsReviewModalVisible(true);
                        }}
                      >
                        Review Application
                      </p>
                    </div>
                    <p
                      style={{
                        color: "black",
                        fontSize: 14,
                        display: "flex",
                        justifyContent: "start",
                        textAlign: "left",
                      }}
                    >
                      Please review the information above and submit the
                      application
                    </p>
                  </Col>
                </Row>
                <Row gutter={24}>
                  <Col xl={24} xs={24}>
                    <S.headingContent
                      style={{ marginLeft: "0", marginBottom: "10px" }}
                    >
                      Terms and Agreements
                    </S.headingContent>
                    <S.scroralbleDiv>
                      <S.scrollableDivContent>
                        {generateTermsAndConditions('NCD Member Agreement', ncdMemberAgreement)}
                          {cartOverview?.cartProducts?.map((element, index) => (
                            <div key={index} style={{ width: "100%" }}>
                              {element.type === "dental" ? (
                                <>
                                  {generateTermsAndConditions('NCD Dental Agreement', ncdDentalAgreement)}
                                  {generateTermsAndConditions('NSBA Agreement', nsbaAgreement)}
                                </>
                              ) : null}
                              {element.type === "vision" ? (
                                <>
                                  {generateTermsAndConditions('NCD Vision Agreement', ncdVisionAgreement)}
                                </>
                              ) : null}
                            </div>
                          ))}
                      </S.scrollableDivContent>
                    </S.scroralbleDiv>
                  </Col>
                  <Col xs={24} xl={24}>
                    <Form.Item
                      name="sign"
                      label="Please Sign Below"
                      rules={[
                        {
                          required: true,
                          message: "Please provide your signature above",
                          validator: signatureValidator,
                        },
                      ]}
                    >
                      {/* <Input
                        placeholder="Sign here"
                        style={{
                          width: "80%",
                          display: "flex",
                          justifyContent: "start",
                        }}
                      /> */}
                    <div style={{ border: '1px solid black', width: '100%'}}><SignatureCanvas ref={(ref) => { setSignature(ref) }} penColor='black' canvasProps={{width: 800, height: 100, className: 'sigCanvas', border: '1px solid black'}} /></div>
                    
                    </Form.Item>
                    <Button onClick={clearSignature} style={{ display: "inline-block", justifyContent: "end", float: 'right' }}>Clear Signature</Button>
                    <Form.Item
                      name="agree"
                      rules={[
                        {
                          validator: validation, 
                        },
                      ]}
                    >
                      <Checkbox
                        style={{
                          margin: "10px 0",
                          display: "flex",
                          justifyContent: "start",
                        }}
                        checked={checked}
                        onChange={onCheckboxChange}
                      >
                        <p
                          style={{
                            color: "black",
                            fontSize: 14,
                            display: "flex",
                            justifyContent: "start",
                            textAlign: "left",
                          }}
                        >
                          <span style={{ color: "red", fontSize: 16 }}>*</span>&nbsp;
                          I accept the&nbsp;
                          <a
                            onClick={() => setIsTermsAndConditionsModalVisible(true)}
                          >
                            Terms and Conditions
                          </a>
                        </p>
                      </Checkbox>
                    </Form.Item>
                    <br />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
      <SummaryModal
        isVisible={isReviewModalVisible}
        setIsVisible={setIsReviewModalVisible}
      />
      <TermsAndConditionsModal
        isVisible={isTermsAndConditionsModalVisible}
        setIsVisible={setIsTermsAndConditionsModalVisible}
      />
      <CheckOutModal
        isVisible={isCheckOutModalVisible}
        setIsVisible={setIsCheckOutModalVisible}
        customFunction={handleAfterCheckOut}
      />
    </>
  );
};

export default SummaryForm;
