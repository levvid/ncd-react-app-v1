import React, { useState, lazy, useEffect } from "react";
import { Form, Input, Select, Row, Col, Radio, Divider } from "antd";
import axios from "axios";
import moment from "moment";
import * as S from "../../styles";
import NcdDatePicker from '../../../../common/DatePicker';
import PreviousCoverageModal from '../previousCoverageModal';
import { useStore } from "../../../../Store";
import configData from "../../../../config";

const SvgIcon = lazy(() => import("../../../../common/SvgIcon"));

const { Option } = Select;
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
    xl: {
      span: 24,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
    xl: {
      span: 24,
    },
  },
};

const FollowUpQuestions = ({ form }) => {
  const { prevInsuranceForm } = useStore((state) => ({
    prevInsuranceForm: state.prevInsuranceForm,
  }));

  return (
    <div>
      <p>Please answer the following questions</p>
      <Row gutter={12} style={{ marginTop: 10 }}>
          <>
            <Divider style={{ margin: "10px 0" }} />
            <Col xl={24} xs={24}>
              <div>
                <b>
                  1. What is the Name of the previous plan?
                </b>
              </div>
              <br />
              <Form.Item
                name="POPC_2"
              >
                <Input style={{ width: "55%" }} placeholder="Enter Name" />
              </Form.Item>
            </Col>
            <Divider style={{ margin: "10px 0" }} />
            <Col xl={18} xs={24}>
              <div>
                <b>2. Effective Date of previous Dental Plan</b>
              </div>
              <br />
              <Form.Item
                wrapperCol={20}
                name="POPC_3"
                initialValue={moment().format('MM/DD/YYYY')}
              >
                <NcdDatePicker 
                  minDate='01/01/1920'
                  maxDate='01/01/2100'
                />
              </Form.Item>
            </Col>
            <Divider style={{ margin: "10px 0" }} />
            <Col xl={18} xs={24}>
              <div>
                <b>3. Termination Date of previous Dental Plan</b>
              </div>
              <br />
              <Form.Item
                name="POPC_4"
                initialValue={moment().format('MM/DD/YYYY')}
              >
                <NcdDatePicker 
                  minDate='01/01/1920'
                  maxDate='01/01/2100'
                />
              </Form.Item>
            </Col>
            <Divider style={{ margin: "10px 0" }} />
          </>
      </Row>
      <Form.Item
        name="POPC_5"
        label="I attest as licensed agent I have verified coverage with client and that their plan has terminated within 30 days of the effective date of this plan"
        rules={[
          {
            required: true,
            message: "You need to agree to proceed",
          },
        ]}
        initialValue={1}
        hidden
      >
        <Radio.Group>
          <Radio value={1}>Yes</Radio>
          <Radio value={0}>No</Radio>
        </Radio.Group>
      </Form.Item>
      <br />
    </div>
  );
};

const PrevInsurance = (props) => {
  const {
    applicantForm,
    searchFields,
    planForm,
    cartOverview,
    appID,
    prevInsuranceForm,
    setPreviousInsuranceForm,
  } = useStore((state) => ({
    applicantForm: state.applicantForm,
    searchFields: state.searchFields,
    planForm: state.planForm,
    cartOverview: state.cartOverview,
    appID: state.appID,
    prevInsuranceForm: state.prevInsuranceForm,
    setPreviousInsuranceForm: state.setPrevInsuranceForm,
  }));

  const [form] = Form.useForm();
  const [formLayout, setFormLayout] = useState("vertical");
  const [followUpQuestions, setFollowUpQuestions] = useState(prevInsuranceForm.prev_dental_insurance === 1);
  const [isPreviousCoverageModalVisible, setIsPreviousCoverageModalVisible] = useState(false);
  
  props.setFormHandler(form);

  const handleAfterNotice = async (success) => {
    if (!success) {
      props.setIsStepSuccessful(false);
      return;
    }
    
    let values = form.getFieldsValue();
    try {
      setPreviousInsuranceForm(values);
      let res = await axios({
        method: "patch",
        url: `${configData.API_URL}/app`,
        data: {
          app_data: {
            searchFields: searchFields,
            planForm: planForm,
            cartOverview: cartOverview,
            applicantForm: applicantForm,
            prevInsuranceForm: values,
          },
          app_email: searchFields.banner_email,
          app_id: appID,
        },
        headers: {
          "Content-Type": "application/json",
        },
      });
      props.setIsStepSuccessful(true);
    } catch (e) {
      props.setIsStepSuccessful(false);
    }
  };

  const onFinish = (values) => {
    let q1 = form.getFieldValue('prev_dental_insurance');
    let q2 = form.getFieldValue('POPC_2') || '';
    if (q1 !== 1 || q2.length > 0) {
      handleAfterNotice(1);
    } else {
      setIsPreviousCoverageModalVisible(true);
    }
  };

  return (
    <>
    <Form
      {...formItemLayout}
      form={form}
      formLayout={formLayout}
      name="applicant"
      onFinish={onFinish}
      size="large"
      initialValues={prevInsuranceForm}
      scrollToFirstError
    >
      <Row gutter={12}>
        <Col xl={24} xs={24}>
          <S.headingDiv>
            <S.bluTag />
            <S.headingContent>Previous Coverage</S.headingContent>
          </S.headingDiv>
          <br />
          <Row gutter={24}>
            <Col xl={24} xs={24}>
              <div>
                <b>
                  <span style={{ color: "red", fontSize: 16 }}>*</span>&nbsp;
                  Are you currently enrolled in a dental plan? If your current
                  plan has more than $1000 in PPO Coverage then your 12 months
                  dental waiting period will be waived!
                </b>
              </div>
              <Form.Item
                name="prev_dental_insurance"
                rules={[
                  {
                    required: true,
                    message: "Please answer this question",
                  },
                ]}
              >
                <Radio.Group
                  onChange={(e) => {
                    setFollowUpQuestions(e.target.value === 1);
                  }}
                >
                  <Radio value={1}>Yes</Radio>
                  <Radio value={0}>No</Radio>
                  <Radio value={-1}>Not Sure</Radio>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
        </Col>
        <Col xl={18} xs={24}>
          {followUpQuestions ? <FollowUpQuestions form={form} /> : null}
        </Col>
      </Row>
    </Form>
    <PreviousCoverageModal 
      isVisible={isPreviousCoverageModalVisible} 
      setIsVisible={setIsPreviousCoverageModalVisible}
      customFunction={handleAfterNotice}
    />
    </>
  );
};

export default PrevInsurance;
