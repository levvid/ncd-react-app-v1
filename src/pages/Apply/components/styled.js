import styled from 'styled-components';
import {Input} from 'antd';
import SvgIcon from '../../../common/SvgIcon';

export const DepBox = styled.div`
    background-color: #F6F7F7;
    padding: 20px 10px;
    margin: 10px 0px;
`;