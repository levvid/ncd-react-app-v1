import styled from 'styled-components';
import MaskedInput from 'antd-mask-input';

export const Container = styled.div`
  float: none;
  margin: 5px auto;
  min-height: 80vh;
  position: relative;
  width: 90%;
  display: flex;
  background-color: #ffffff;
  justify-content: flex-start;
  padding-top: 10px;
  padding-bottom: 20px;
  text-align: left;
  overflow: hidden;
  max-width: 1300px;
  @media only screen and (max-width: 768px) {
    width: 95%;
  }
`;

export const bluTag=styled.div`
position: relative;
background: rgb(39,140,192);
width:1%;
/* height:100% */
`;

export const PaymentBox=styled.div`
  background-color: #f6f7f7;
  padding: 20px 30px;
`;

export const headingContent=styled.div`
display: flex;
align-items: center;
margin-left: 20px;
@media only screen and (max-width: 768px) {
  margin-left: 5px;
  font-size: 15px;
}
color: black;
font-weight: 700;
font-size: 17.5px;
`;


export const headingDiv=styled.div`
min-height: 45px;
background: rgb(246,246,246);
display: flex;
justify-content: start;
border-radius: 3px;
padding: 10px auto;
overflow: hidden;
margin-top: 10px;
`;


export const tableColHeading=styled.div`
margin:0 20px;
color: black;
font-weight: 500;
font-size: 14.5px;
`;


export const tableHeadingDiv=styled.div`
min-height: 40px;
background: rgb(246,246,246);
display: flex;
align-items: center;
justify-content: space-between;
border-radius: 3px;
padding: 10px auto;
overflow: hidden;
`;

export const tableRowDiv=styled.div`
min-height: 45px;
display: flex;
align-items: center;
justify-content: space-between;
border-radius: 3px;
padding: 10px auto;
overflow: hidden;
`;

export const fieldName=styled.div`
margin:0 20px;
color: black;
font-weight: ${props=>props.fontWeight||500};
font-size: 15.5px;
`;

export const tableFooterDiv=styled.div`
min-height: 40px;
background: rgb(219,242,250);
display: flex;
align-items: center;
justify-content: flex-end;
border-radius: 3px;
padding: 10px auto;
overflow: hidden;
`;

export const tableFooterContent=styled.div`
margin:0 20px;
color: black;
font-weight: 500;
font-size: 15.5px;
`;

export const SpinnerContainer = styled.div`
  .spinner-container {
    position: relative;
    margin-top: 15vh;
    z-index: 100;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;
export const modalRowDiv=styled.div`
min-height: 25px;
display: flex;
align-items: center;
justify-content: space-between;
border-radius: 3px;
padding: 10px auto;
overflow: hidden;
`;

export const modalfieldName=styled.div`
margin:0 20px;
color: black;
font-weight: ${props=>props.fontWeight||500};
font-size: 12.5px;
`;

export const scroralbleDiv=styled.div`
border:1.3px solid black;
border-radius: 4px;
max-height: 200px;
overflow: auto;
`;

export const scrollableDivContent=styled.div`
margin-left: 10px;
color: black;
font-weight: 500;
font-size: 10.5px;
`;

export const NcdMaskedInput = styled(MaskedInput)`
  .ant-input::placeholder {
    color: rgb(0,73,122) !important;
  }
  .ant-input-prefix {
    color: rgb(0,73,122);
  }
`;


export const termsAndConditionsHeader=styled.div`
  margin-top: 10px;
  font-weight: 600;
  font-family: 'Montserrat',sans-serif;
  color: #084566;
  line-height: 1.0rem;
  margin-bottom: 0.5em;
  font-size: 2em;
  text-align: center;
`;

export const termsAndConditionsContent=styled.div`
  margin-left: 10px;
  color: black;
  font-weight: 500;
  font-size: 10.5px;
`;
