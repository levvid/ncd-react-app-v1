import { useState, useEffect, lazy } from "react";
import queryString from "query-string";
import axios from "axios";
import { Link } from "react-router-dom";
import {
  Steps,
  message,
  Button,
  Row,
  Col,
  Spin,
  Result,
  Typography,
} from "antd";
import Sticky from 'react-sticky-el';
import configData from "../../config";
import * as S from "./styles";
import { useStore } from "../../Store";
import { NcdPrimaryButton } from "../../common/Button";
import { CloseCircleOutlined } from "@ant-design/icons";
import ApplicantComponent from "./components/forms/ApplicantInformationComponent";
import PrevInsuranceComponent from "./components/forms/PrevInsurance";
import PaymentInfoComponent from "./components/forms/PaymentInfo";
import SummaryComponent from "./components/forms/Summary";
import SuccessPage from './components/SuccessPage.js';

const { Paragraph, Text } = Typography;

//CART
const CartCard = lazy(() => import("./components/Cart"));
const ChatCard = lazy(() => import("../../components/Chat"));

const { Step } = Steps;

const ApplyComponent = (props) => {
  const {
    searchFields,
    setAppID,
    setCartOverview,
    setSearchFields,
    setPlanForm,
    setApplicantForm,
    setPrevInsuranceForm,
    setPaymentInfoForm,
    setCurrent,
    current,
    appID,
  } = useStore((state) => ({
    searchFields: state.searchFields,
    setAppID: state.setAppID,
    setCartOverview: state.setCartOverview,
    setSearchFields: state.setSearchFields,
    setPlanForm: state.setPlanForm,
    setApplicantForm: state.setApplicantForm,
    setPrevInsuranceForm: state.setPrevInsuranceForm,
    setPaymentInfoForm: state.setPaymentInfoForm,
    current: state.current,
    setCurrent: state.setCurrent,
    appID: state.appID,
  }));

  const [isProcessing, setIsProcessing] = useState(false);
  const [isStepSuccessful, setIsStepSuccessful] = useState(null);
  const [isFetchingData, setIsFetchingData] = useState(true);
  const [isLinkInvalid, setIsLinkInvalid] = useState(false);
  const [isApplicationSuccessful, setIsApplicationSuccessful] = useState(false);
  const [nextText, setNextText] = useState('Continue');
  const isMobile = window.innerWidth <= 768;

  useEffect(async () => {
    let parsed = queryString.parse(window.location.search);
    let app_id = parsed.app_id;
    const currentPage = parseInt(parsed.current);
    setAppID(app_id);
    setCurrent(currentPage);

    props.history.push(`/apply?app_id=${app_id}&current=${currentPage}`);
    try {
      let res = await axios({
        method: "get",
        url: `${configData.API_URL}/app/${app_id}`,
      });
      let app_data = res.data["Item"]["app_data"];
      setSearchFields(app_data.searchFields || {});
      setPlanForm(app_data.planForm || {});
      setCartOverview(app_data.cartOverview || {});
      setApplicantForm(app_data.applicantForm || {});
      setPrevInsuranceForm(app_data.prevInsuranceForm || {});
      setPaymentInfoForm(app_data.paymentInfoForm || {});
      setIsFetchingData(false);
    } catch (e) {
      if (e.response.data.msg === 'APP_COMPLETE') {
        setIsApplicationSuccessful(true);
      } else {
        setIsLinkInvalid(true);
      }
    }
  }, []);

  useEffect(async () => {
    let parsed = queryString.parse(window.location.search);
    let app_id = parsed.app_id;
    try { // MLG
      setInterval(async () => {
        let r = await axios({
          method: 'post',
          url: `${configData.API_URL}/poll`,
          data: {app_id: app_id}
        })
      }, 60*1000);
    } catch(e) {
      console.log("Error: " + JSON.stringify(e));
    }
  }, []);

  useEffect(() => {
    if (isStepSuccessful) {
      if (current < steps.length - 2) {
        setCurrent(current + 1);
        setIsStepSuccessful(false);
        props.history.push(`/apply?app_id=${appID}&current=${current + 1}`);
      } else {
        setIsApplicationSuccessful(true);
      }
    }
    window.scrollTo(0, 0);
    setIsProcessing(false);
    setIsStepSuccessful(null);

  }, [isStepSuccessful]);

  const formHandlers = [];

  useEffect(() => {
    if (current === steps.length - 2) {
      setNextText('Submit Application');
    } else {
      setNextText('Continue');
    }
  }, [current]);

  const next = async (scrollDir=0) => {
    try {
      await formHandlers[current].validateFields();
      await formHandlers[current].submit();
      setIsProcessing(true);
    } catch (e) {
      let errorFields = await formHandlers[current].getFieldsError();
      for (let i=0; i<errorFields.length; i++) {
        if (errorFields[i].errors.length > 0) {
          formHandlers[current].scrollToField(errorFields[i].name);
          if (scrollDir === 0) {
            window.scrollBy(0, -60);
          } else {
            window.scrollBy(0, 60);
          }
          break;
        }
      }
      message.error("Please check the form fields");
    }
  };

  const prev = async () => {
    setCurrent(current - 1);
    props.history.push(`/apply?app_id=${appID}&current=${current - 1}`);
    window.scrollTo(0, 0);
  };

  const done = async () => {
    try {
      await formHandlers[current].validateFields();
      await formHandlers[current].submit();
      setIsProcessing(true);
    } catch (e) {
      message.error("Please check the form fields");
    }
  };

  const nextFunction = () => {
    if (current === steps.length-2) {
      return done();
    } else {
      return next(1);
    }
  };

  let steps = [];

  if (['dental', 'both'].includes(searchFields.search_product_type)) {
    steps = [
      {
        title: "Applicant",
        content: (
          <ApplicantComponent
            setFormHandler={(f) => (formHandlers[0] = f)}
            setIsStepSuccessful={setIsStepSuccessful}
          />
        ),
      },
      {
        title: "Coverage",
        content: (
          <PrevInsuranceComponent
            setFormHandler={(f) => (formHandlers[1] = f)}
            setIsStepSuccessful={setIsStepSuccessful}
          />
        ),
      },
      {
        title: "Payment",
        content: (
          <PaymentInfoComponent
            setFormHandler={(f) => (formHandlers[2] = f)}
            setIsStepSuccessful={setIsStepSuccessful}
          />
        ),
      },
      {
        title: "Summary",
        content: (
          <SummaryComponent
            setFormHandler={(f) => (formHandlers[3] = f)}
            setIsStepSuccessful={setIsStepSuccessful}
          />
        ),
      },
      {
        title: "Completed",
        content: (
          <SuccessPage />
        ),
      },
    ];
  } else {
    steps = [
      {
        title: "Applicant",
        content: (
          <ApplicantComponent
            setFormHandler={(f) => (formHandlers[0] = f)}
            setIsStepSuccessful={setIsStepSuccessful}
          />
        ),
      },
      {
        title: "Payment",
        content: (
          <PaymentInfoComponent
            setFormHandler={(f) => (formHandlers[1] = f)}
            setIsStepSuccessful={setIsStepSuccessful}
          />
        ),
      },
      {
        title: "Summary",
        content: (
          <SummaryComponent
            setFormHandler={(f) => (formHandlers[2] = f)}
            setIsStepSuccessful={setIsStepSuccessful}
          />
        ),
      },
      {
        title: "Completed",
        content: (
          <SuccessPage />
        ),
      },
    ];
  }

  return (
    <S.Container>
      <Row justify="center" gutter={24} style={{ width: "100%", margin: 0}}>
        <Col xs={24} xl={16} style={{minHeight: 1000}}>
          {!isFetchingData && !isApplicationSuccessful ? (
            <Row gutter={24}>
              <Col
                xs={24}
                xl={24}
                style={{ marginBottom: "40px", marginTop: "20px" }}
              >
                <Steps progressDot current={current} responsive={true}>
                  {steps.map((item) => (
                    <Step key={item.title} title={item.title} />
                  ))}
                </Steps>
              </Col>
              <Col xs={24} xl={24}>
                <div className="steps-content">{steps[current].content}</div>
                
                  <div className="steps-action" style={{width: '100%'}}>
                  <Row gutter={24} justify='end'>
                    {current > 0 && current < steps.length - 1 && (
                      <Col xl={12} xs={12}>
                        <NcdPrimaryButton
                          style={{ float: "left", margin: "0",}}
                          size="large"
                          onClick={() => prev()}
                        >
                          Previous
                        </NcdPrimaryButton>
                      </Col>
                    )}
                    {current < steps.length - 2 && (
                      <Col xl={12} xs={12}>
                        <NcdPrimaryButton
                          type="primary"
                          style={{ float: "right", margin: "0", backgroundColor: '#fa8406'}}
                          size="large"
                          onClick={() => next()}
                          loading={isProcessing}
                        >
                          Continue
                        </NcdPrimaryButton>
                      </Col>
                    )}
                    {current === steps.length - 2 && (
                      <Col xl={12} xs={12}>
                        <NcdPrimaryButton
                          type="primary"
                          style={{ float: "right", margin: "0", backgroundColor: '#fa8406'}}
                          size="large"
                          onClick={() => done()}
                          loading={isProcessing}
                        >
                          Submit Application
                        </NcdPrimaryButton>
                      </Col>
                    )}
                    </Row>
                  </div>
              </Col>
            </Row>
          ) : (
            <>
              {!isLinkInvalid && !isApplicationSuccessful ? (
                <S.SpinnerContainer>
                  <div className="spinner-container">
                    <Spin size="large" />
                    <p>Loading your application...</p>
                  </div>
                </S.SpinnerContainer>
              ) : (
                <>
                  {!isApplicationSuccessful ? (
                    <Result
                      status="error"
                      title="Invalid application link"
                      extra={[
                        <Link to="/">
                          <Button type="primary">
                            Start a new application
                          </Button>
                        </Link>,
                      ]}
                    >
                      <div className="desc">
                        <Paragraph>
                          <Text
                            strong
                            style={{
                              fontSize: 16,
                            }}
                          >
                            This could be due to:
                          </Text>
                        </Paragraph>
                        <Paragraph>
                          <CloseCircleOutlined className="site-result-demo-error-icon" />{" "}
                          You copied the wrong link. Please check your email for
                          the correct link.
                        </Paragraph>
                        <Paragraph>
                          <CloseCircleOutlined className="site-result-demo-error-icon" />{" "}
                          The link got expired.
                        </Paragraph>
                      </div>
                    </Result>
                  ) : <SuccessPage />}
                </>
              )}
            </>
          )}
        </Col>
        <Col xs={24} xl={8}>
          {!isMobile ? (
            <Sticky>
              <>
                {!isFetchingData && !isApplicationSuccessful ? <CartCard nextFunction = {nextFunction} loading={isProcessing} nextText={nextText} /> : null}
                <ChatCard />
              </>
            </Sticky>
          ) : (
            <>
              {!isFetchingData && !isApplicationSuccessful ? <CartCard nextFunction = {nextFunction} loading={isProcessing} nextText={nextText} /> : null}
              <ChatCard />
            </>
          )}
        </Col>
      </Row>
    </S.Container>
  );
};

export default ApplyComponent;
